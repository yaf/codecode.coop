---
layout: temoignage
nom: Scopyleft
homepage: http://scopyleft.fr
---

## Qui sommes-nous ?

### Un collectif

Au départ, des amis qui ont envie de travailler ensemble. Et plus tard, des personnes qui partagent une certaine vision de _l'entreprise_ des relations inter-personnelles.

Nous ne travaillons pas forcement ensemble pour un client (ça arrive malgré tout). Nous travaillons ensemble à faire en sorte que chacun d'entre nous se sente bien, fasse des choses qui lui conviennent.


Scopyleft est une société coopérative ouvrière de production à responsabilité limitée et à capital variable.


### Ouverture et partage

Au début de Scopyleft, il y a beaucoup de transparence : la prise de décision, des éléments comptables, des comptes-rendus, des questionnements.

C'est un souhait toujours très présent. Et cette plateforme et un moyen de plus pour partager des idées, des façons de faire, pour donner envie, pour montrer des exemples, prouver que c'est possible.


## Vie de tous les jours

Historiquement, les membres de Scopyleft vivent dans le sud. Principalement à Montpellier (l'un d'entre eux vit à Arles). Ils se retrouvent ensemble dans une maison, font des excursions dans les montagnes.

Aujourdh'ui, l'un vit à Montreal, deux sont nomades (mais pas sur les mêmes grilles de voyage), un vit en grande ceinture parisienne et un autre est entre Perpignan et Montpellier (et parfois un peu en balade ailleurs, c'est pas très clair :D).

Nous travaillons principalement dans des équipes distribuées ou pour des clients qui sont ok avec le travail à distance. Les nomades se retrouvent souvent à aller à la rencontre des clients, de manière ponctuelle.

Nous effectuons un point de synchro une fois par semaine, une heure en visio (parfois 30 minutes seulement). Il nous arrive de caler des points intermédiaire sur des sujets précis. Nous nous apprêtons à expérimenter de prendre une demie-journée pour le collectif.

Un gros avantage à créer des rituels, c'est qu'on peut rater facilement une semaine, ce n'est pas un soucis, la semaine suivante arrive vite.

Ces instances fonctionnent parce que nous ne sommes pas trop nombreux pour le moment, Qu'en sera-t-il si nous accueillons de nouvelles personnes dans le collectif ? C'est une question qui nous anime parfois. Et nous allons sans doute l'expérimenter prochainement.

Nous utilisons [Keybase](https://keybase.io/) pour nos échanges au quotidien. Ça nous permet d'avoir un espace chiffré pour partager certaines informations sensibles. Nous avons un compte sur [github](https://github.com/scopyleft/), un sur [gitlab](https://gitlab.com/scopyleft), et nous sommes en train d'expérimenter l'auto-hébergement avec un [gitea](https://git.scopyleft.fr/).


## Répartition des revenus

Chez Scopyleft, nous nous payons le salaire dont nous avons besoin. Nous en discutons ensemble, et actuellement chacun d'entre nous se paie le salaire qu'il souhaite, indépendamment de ce qu'il facture pour la SCOP.

Le surplus est distribué une fois par an en part travail comme habituellement en SCOP il me semble.

