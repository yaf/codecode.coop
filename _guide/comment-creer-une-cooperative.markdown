---
layout: default
title:  "Comment créer une coopérative"
date:   2019-10-04 17:20:00 +0200
categories: main
---

# Comment créer une coopérative de travailleurs

Dans un premier temps voici quelques ressources pour vous aider dans votre création d’entreprise. Nous vous recommandons de les télécharger :

_À compléter_

Vous remarquerez que les prochaines pages sont essentiellement une bibliographie annotée. C’est parce que nous ne pouvons pas traiter tous les sujets importants détaillants les étapes de création d’une entreprise dans ce guide ; tout du moins pas au niveau de détail requis pour que fassiez des choix assez éclairés pour démarrer votre entreprise. Heureusement d’autres personnes ont fait ce travail, nous allons donc décrire le processus et renvoyer vers eux pour les détails.

## 1. Trouver des coopérateurs

C’est bon vous êtes motivés pour créer une coopérative. Chouette ! Malheureusement une personne seule ne pas être une coopérative. Vous allez avoir besoin de partenaires – au moins deux – et préférablement trois à cinq ou plus. Ça peut être des amis, des collègues, ou bien n’importe quelle personne avec qui vous avez envie de travailler. Si vous ne connaissez personne, vous pourrez peut être en trouver dans des groupes d’indépendents de la région où vous êtes, ou bien dans des groupes sociaux, dans des meetups, ou bien dans des mailing-list. Faites savoir que vous voulez créer une coopérative, et ils viendront sûrement à vous.

## 2. Consolider une vision claire

Rassemblez maintenant votre groupe, dites pourquoi vous voulez créer une coopérative, et écoutez les avis des autres personnes – c’est un projet collectif ! Il y a aura surement différents avis sur ce que doit faire une coopérative, ce que vous allez créer sera donc peut être différent de ce que vous aviez en tête au début. Restez flexible et ouvert aux nouvelles idées, encore une fois c’est un travail collectif. Par exemple, réduire les coûts en se regroupant sur des besoins communs pourra être très important pour certains, alors que pour d’autres personnes la principale motivation pour se regrouper pourra être d’avoir une équipe assez diverse pour porter des projets plus importants. Allez vous être un groupe de travailleurs et travailleuse qui font plus ou moins la même chose, ou alors allez vous avoir différentes spécialités ? Comment le travail va t-il être réparti ? Quel type de clients allez-vous avoir ? Pourquoi ? La diversités des intérêts n’est pas forcément mauvaise – vous allez peut être devoir lancer plusieurs coopératives, pas juste une !

Avant de commencer, il est préférable d’avoir écrit une vision de ce qui va vous porter en tant que groupe. Vous n’avez pas besoin d’être très spécifique, mais c’est important que chaque personne comprenne ce que la coopérative va faire, et pourquoi il faut bien le faire ensemble qu’en tant qu’indépendent. Il est aussi important que chacun comprennent les concepts de base de la démocratie contrôlée par les salarié·es (pouvoir partage, responsabilité partagée) et le partage de la possession de l’entreprise (risque partagé, gain partagé).

## 3. Définissez la manière dont vous allez travailler

Vous allez bientôt devoir prendre des décisions. Avant ça, il faut définir la façon dont le groupe va prendre des décisions. Utilisez cette manière de travailler pour créer votre coopérative, vous pourrez ainsi tester votre façon de travailler ensemble, la faire évoluer, et peut être l’adopter comme modèle de gouvernance de votre coopérative. Vous devrez répondre à deux questions :

1. Qui prend les décisions ?
2. Comment sont-elles prises ?

Plus spécifiquement, quelles sont les décisions qui nécessitent une discussion de groupe ? Quelles types de décisions peuvent être prises individuellement ou en plus petit groupe ? Les décisions se prennent-elles avec un vote à la majorité ? Avec une majorité supérieure (75 % par exemple) ? Ou bien une décision ne se prend que s’il y a consensus ? Le processus de décision doit être très clair, surtout sur les quorums, sur ce qu’il se passe en cas d’égalité, les limites de temps d’un débat, ou tout autre détail qui vous semble important.

La démocratie directe est la norme dans les petites coopératives ouvrières, mais tout le monde doit adhérer à l’idée avant de commencer – ça peut fonctionner très bien si les gens sont très engagés dans ce mode de fonctionnement, mais très mal si tout le monde n’est pas à fond dedans. Le processus de prise de décision c’est bien plus que juste compter des votes. Vous devez penser « l’inclusivité, votre engagement dans le processus démocratique, la relation entre les personnes du groupe, et tous les aspects de la délibération: droits oraux et responsabilité (paramètres de l’agenda, reformulation, articulation, persuasion, vote, et dissidence) et droit d’écoute et reponsabilités (compréhension et considération) » \[Gastil p.16\]. Prenez du temps pour développer les façons de sortir d’une situation qui parait inextricable.

Bibliographie suggérée :

_Introduction aux décisions par consensus, Rachel Williams et Andrew McLeod, Cooperative Starter Series, 2006 (en anglais) &lt;[http://nwcdc.coop/wp-content/uploads/2012/09/CSS08-Intro-to-Consensus-DM.pdf](http://nwcdc.coop/wp-content/uploads/2012/09/CSS08-Intro-to-Consensus-DM.pdf)&gt;_

_Democracy In Small Groups: Participation, Decision Making & Communication., John Gastil, New Society Publishers, Gabriola Island, BC. 1993._

_Facilitator’s Guide to Participatory Decision-Making, Kaner, Sam, et al, New Society Publishers, Gabriola Island, BC. 1996_

## 4. Créer un plan de démarrage

C’est bon vous savez avec qui vous voulez travailler, votre vision, et savez à peu près ce que vous voulez faire ensemble. Vous pouvez maintenant commencer votre premier projet de groupe : passer d’un groupe de personnes intéressées à une coopérative fonctionnelle !

Passez du temps à étudier les étapes 5 à 10, et faites en un vrai plan de démarrage. Transformez les dans des petites tâches actionnables, donnez vous des objectifs datés, distribuez les tâches aux personnes en fonction de leurs compétences, de leur expérience, de leurs envies. C’est comme ça que vous allez tester (et ajuster) le processus de décision que vous avez créé dans les étapes précédentes. Un facilitateur ou un coordinateur pourrait être utile pour vous orienter et assurer que les choses se passent bien et soient effectivement réalisées. Assurez-vous que tout le monde apprenne le fonctionnement au fur et à mesure.

## 5. Comprenez qui vous êtes et ce que vous pouvez faire

Vous êtes un groupe d’indépendents, et vous savez donc ce que vous savez faire. En êtes vous vraiment surs ? La plupart d’entre vous avez sûrement des compétences et des expériences que vous ne mettez pas en avant, peut être avez vous oublié que vous avez certaines expertises. Votre coopérative aura besoin d’une réflexion globale, elle sera d’autant meilleure si chacun connait les compétences de tout le monde. Des expériences dans les domaines du marketing, de l’écriture de proposition commerciales, de comptabilité, d’administration, de négociation, de facilitation de réunion, de support technique et même en politique peuvent être mises en valeur dans la coopérative.

Avant d’aller plus loin, prenez du temps pour vraiment comprendre les compétences de chacun. Faites une étude qui va plus loin que les CV, et posez des questions ouvertes. Vous aurez peut être de bonnes surprises, vous serez impressionné par toutes les capacités que vous aurez en tant que groupe. Cela ne vous permet pas seulement de connaitre vos capacités, mais facilitera aussi la cohérence du groupe. Vous pourrez aussi avoir de nouvelles idées de marchés, de services. Grâce à ça vous pourrez envisager des choses que vous n’auriez pas pu envisager en tant qu’indépendent.

## 6. Test de faisabilité : regardez avant de sauter

Tout le monde est maintenant confiant. En tant que groupe d’indépendents, vous avez probablement tout ce qu’il faut pour réussir. Habituellement si vous créez une entreprise de zéro, vous allez préparer (ou demander à quelqu’un de faire) une étude de marché et une étude de faisabilité, pour vraiment étoffer et quantifier votre grand projet. Si vous n’avez pas assez de clients ou bien que vos dépenses sont trop élevées, vous voulez le savoir le plus tôt possible pour ne pas gâcher du temps et de l’argent sur un projet qui ne fonctionne pas. Dans votre cas, il se peut que vous soyez en train de créer une entreprise qui ressemble beaucoup au travail que vous faisiez en tant qu’indépendants – mais maintenant en collectif –, vous aurez donc une bonne idée de ce qui est faisable. Mais si vous ajoutez de nouveaux éléments à votre modèle commercial, vous devrez réfléchir à comment cela fonctionnera financièrement. Dans tous les cas, l’étude de faisabilité (qu’elle soit très formelle ou assez informelle, en fonction de vos besoin) est un moment important du développement de votre coopérative.

Alors, il est maintenant temps de faire le grand saut. Il ne suffit pas d’être conscient du risque que vous prenez, il faut surtout que tous les membres soient conscients de toutes les opportunités qu’ils et elles ont. Une autre raison de faire ça est pour que tout le monde ait le même niveau d’information – vous devez être certain que toutes les personnes vont avoir les mêmes présupposés sur les nouveaux processus de la coopérative, et que tout le monde ait les mêmes attentes. Faites une analyse de type FFOM – référencez vos Forces, Faiblesses, Opportunités et Menaces (l’étape 5 vous aidera pour ça), réfléchissez à comment atténuer les aspects négatifs et tirez le maximum du positif. Qu’est-ce qui va changer ? Aurez-vous besoin de locaux, de nouveaux équipements, de comptabilité ? Allez-vous commencer dans une petite pièce, puis après avoir des bureaux ? Ou bien allez vous faire directement de gros investissements ?

Rassemblez les personnes qui aiment bien les chiffres, faites un sous-groupe, passez du temps sur des tableurs, puis présentez votre travail pour le reste des membres. Faites particulièrement attention à la capitalisation – même si vous n’avez pas besoin d’un gros investissement de la part de chaque personne, l’engagement qui va avec la capitalisation est le cœur du succès de votre coopérative. Revérifiez vos marges, vos dépenses, les impôts… Faites des estimations prudentes concernant vos dépenses et vos recettes. Créez des scénarios optimistes et pessimistes, et assurez-vous que tout le monde soit au courant, et soit engagé, dans ces risques partagés.

Vous allez pouvoir vous appuyer sur ce que vous avez fait dans cette étape pour élaborer votre modèle commercial.

## 7. Modèle commercial

Si vous avez suivi toutes les étapes précédentes, celle ci consistera essentiellement à tout rassembler. Un modèle commercial n’apprendra rien de nouveau à vos membres fondateurs, mais rassembler toutes les pièces peut vous alerter sur des points où vous auriez encore besoin de préparation. C’est aussi utile si quelqu’un veut vous rejoindre à cette étape : la personne pourra consulter votre modèle commercial, et devrait pouvoir comprendre ce que vous voulez faire. Cela vous sera aussi demandé si vous voulez contracter un prêt bancaire (Mais encore une fois, si vous voulez commencer petit, vous n’aurez pas forcément besoin d’un modèle commercial très détaillé.) Il est  important que ce document explique clairement les risques financiers ou autres pour que les nouveaux membres comprennent dans quoi ils et elles s’engagent lorsqu’ils investiront du temps, du capital ou de l’énergie dans la coopérative.

Un modèle commercial classique est composé d’une note de synthèse, d’un résumé d’activité, du détail des produits et services, d’un plan de mise sur le marché, d’une explication du management, d’un plan financier et tout autre document annexe éventuel. En tant que groupe d’indépendent·es, vous devriez être capables de réaliser ce document (mais une vision externe apportera une crédibilité supplémentaire, et pourra être cruciale si vous voulez des financements. Si vous voulez employer quelqu’un pour faire ça, commencez les premières étapes et laissez les étapes de finalisation à un professionel).

## 8. Organisation

Il est maintenant de choisir la forme de votre entreprise. Allez-vous être une association ? Une SA ? Une SARL ? Allez-vous être une SCOP ? Vous devez peser le pour et le contre de toutes ces formes de structure d’entreprise. Il est très importants de faire attention aux aspects suivants : comment la forme d’entreprise peut engager les propriétaires de l’entreprise, est-ce qu’il y a une séparation des biens et des personnes ? Comment les bénéfices sont-ils imposés ? Cette structure d’entreprise est-elle facile à monter ? Est-ce qu’il est facile pour une nouvelle personne de rentrer au capital ? Vous pouvez aller voir des coopératives existantes pour qu’elles vous partagent leur expérience. Vous pouvez aussi aller voir un·e juriste ou un·e avocat·e spécialisé·e dans les coopératives pour obtenir des conseils.

Une fois la forme de l’entreprise choisie, vous devrez écrire les status de l’entreprise qui détaillent la gouvernance et la façon dont votre entreprise fonctionne. Ces documents devront parler entre autre de la capitalisation initiale, qui possède l’entreprise, comment les décisions sont prises, la distribution des profits, la succession, la sortie des membres, la procédure de dissolution. Ça peut être fastidieux, mais ces précautions pourront vous éviter de futurs problèmes. Certaines personnes aiment faire ce genre de documents ; essayez de les trouver parmi votre groupe. S’il n’y a personne, demandez à un juriste ou un avocat de vous aider, ou bien lisez les documents rédigés par d’autres coopératives.

Si vous avez rédigé vous mêmes vos documents, et que vous n’êtes pas entièrement confiants, faites les relire par un·e juriste.

## 9. Dernières actions administratives

Vous pouvez maintenant entamer les démarches administratives : déclarer votre entreprise, ouvrir votre compte en banque… Vous devez aussi commencer à réunir le capital auprès des membres fondateurs. Faites attention à ne pas oublier d’étapes. Vous pouvez aussi vous faire aider par un service externe si vous n’êtes pas sûr de vous.

## 10. Opérations

C’est fait ! Enfin, c’est à dire que ça commence tout juste. Allez annoncer au monde comme vous êtes fier·es de ce vous avez fait, et dites-le à vos clients. Allez en démarcher de nouveaux, vous pouvez leur dire que vous êtes en coopérative, certains seront attirés par la démarche, ils vous perceveront comme démocratique, personne, dignes de confiance, responsable, éthique, et plein d’autres choses !

Il est probable que certaines choses ne deviennent claires que quand vous commencerez à travailler ensemble : certaines choses que vous aviez imaginé ne fonctionneront pas exactement comme vous l’aviez pensé. C’est normal, mais écrivez ces nouvelles façons de fonctionner. Assurez-vous que toute la coopérative se rencontre régulièrement, ou tout de moins reste en contact. Faites des comptes-rendus des réunions, rendez-les disponibles à tous les membres, et conservez-les.

Assurez vous que tous les membres aient accès à toutes les informations, les stratégies, les procédures, les status financiers… La plupart de vos membres trouveront gratifiant d’avoir accès toutes ces informations, et cela renforcera votre esprit d’équipe. Pour l’entraide et la solidarité, contactez d’autres coopératives de travailleurs dans votre branche ou votre région. S’il y a un groupe ou une fédération de coopératives dans votre région, rejoignez-la !


### Section suivante : [Témoignages]({% link temoignages.html %})
