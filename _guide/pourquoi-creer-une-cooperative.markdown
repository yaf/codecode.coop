---
layout: default
title:  "Pourquoi créer une coopérative ?"
date:   2019-10-03 15:00:00 +0200
categories: main
---

# Pourquoi créer une coopérative à partir d’un groupe de travailleurs indépendants ?

À première vue, il semble y avoir une contradiction entre les mots _groupe_ et _indépendants_. Pourtant, d’après notre expérience, ils peuvent très bien se conjuguer.

Beaucoup d’entre nous avons été des personnes salariées, puis indépendantes, puis travailleuses-actionnaires d’une coopérative. Nous avons trouvé que cette dernière formule offre le meilleur des deux mondes : on conserve toujours la liberté d’être son propre patron·ne, mais avec l’aide d’autres personnes pour s’entourer. Voici certains des avantages que vous aurez en étant salarié·e-actionnaire plutôt qu’indépendant·e.

## Quelqu’un pour vous épauler

Trop souvent en tant qu’indépendant·e, soit vous avez trop de travail et vous ne pouvez pas tout assumer, soit vous n’en avez pas. Pendant les périodes de forte activité, ou lors de vos congés, vos collègues qui connaissent votre client peuvent vous offrir un relais – en tant que membre de votre coopérative, ils possèdent la même compétence et instaurent le même respect que vous.

## Pouvoir assurer une présence 24/7 tout en protégeant sa vie hors-travail

Ceux d’entre nous qui assurent un service à haute disponibilité reçoivent des appels à n’importe quelle heure du jour ou de la nuit peuvent partager cette charge. Il sera donc plus facile d’avoir une vie de famille, ou bien de reprendre des études. Et puisqu’une coopérative est par nature coopérante, tout le monde peut partager de manière équitable les permanences nocturnes – au lieu que ce soit toujours fait par les mêmes personnes.

## Mettre à profit les compétences complémentaires

En avançant dans nos carrières, nous avons tous tendance à nous spécialiser, et sans doute plus qu’on ne le devrait. Une rupture technologique ou industrielle peut balayer nos compétences entretenues soigneusement au fil des ans. En faisant partie d’une équipe, nous sommes proches de personnes qui disposent de compétences proches mais distinctes, et la pollinisation croisée qui s’opère naturellement profite à tous. À l’inverse d’une entreprise traditionnelle, le destin partagé d’une coopérative de travailleurs encourage le partage de compétences entre professionnels, qui dans d’autres contextes pourraient se considérer comme concurrents.

## Économie d’échelle sur les frais généraux et administratifs, et plus de choix dans les fonctions supports

Allier ses forces peut permettre des réduire les frais généraux en mutualisant vos dépenses pour des espaces de bureaux, des systèmes d’information, de comptabilité, de conseils juridiques, et de tout autre outil dont vous avez besoin pour gérer votre entreprise. Peu importe ce que votre coopérative va devenir, elle représente naturellement un groupe d’achat. (Ou un groupe de *finalement-on-a-pas-besoin-d-acheter-ça* lorsque vous découvrez que vos nouveaux collègues ont des compétences que vous déléguiez auparavant.)

## La possibilité d’avoir des clients aux besoins plus importants

Avez-vous déjà dû transférer du travail à quelqu’un d’autre parce que la tâche à réaliser demandait plus de travail que ce que vous étiez capable de fournir ? Ou regretter de refuser un contrat parce qu’il était au-delà de vos possibilités ? En tant que membre d’une coopérative vous pourrez avoir plus d’opportunités, parce que vous ne serez plus seul pour faire tout le travail. Aussi avec la coopérative votre rapport au travail sera plus flexible et beaucoup moins tout ou rien par rapport à un emploi traditionnel ; il existe des façons très variées de faire fonctionner une coopérative, pour s’adapter aux besoins de ses membres. Par exemple, une coopérative peut permettre à ses membres de continuer à proposer des prestations en solo (avec leurs anciens clients, ou de nouveaux), tout en acceptant de nouvelles prestations plus importantes au travers de la coopérative, sur lesquelles ils s’engagent à travailler.

## Un plus grand nombre et une plus grande diversité de prestations à choisir

Une coopérative de cinq personnes peut faire cinq fois plus qu’un·e indépendent·e, et cette plus grande capacité de travail augmente la visibilité sur le marché et sur les clients. Votre présence accrue vous apportera des opportunités de meilleure qualité, avec plus de cerveaux pour réfléchir à comment en bénéficier.

## Camaraderie professionnelle

Les situations les plus difficiles deviennent plus simple à appréhender quand plusieurs cerveaux, yeux, et perspectives les appréhendent. Les échanges et la solidarité rendent les tâches rébarbatives moins pénibles, plus enrichissantes, et plus plaisantes.

## Plus que la somme des ses parties

Si le nombre de membres augmente arithmétiquement, le travail d’équipe augmente géométriquement. Pour chaque nouvelle personne qui rejoint la coopérative, ce sont plus qu’une seule nouvelle dynamique interpersonnelle, plus qu’une seule nouvelle idée, et plus qu’un seul angle de vue qui s’ajoutent. De plus, les groupes prennent généralement des décisions mieux informées, plus prudentes, donc finalement meilleures que celles prises par une seule personne.

### Section suivante : [Qu’est-ce qu’une coopérative de travailleurs ?]({% link _guide/qu-est-ce-qu-une-cooperative-de-travailleurs.markdown %})
